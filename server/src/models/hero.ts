 
class Hero {

  id: string;

  alias: string;
 
  name: string;
 
  photo: string;

  bio: string;

}

export default Hero;