const puppeteer = require("puppeteer");
const { v4: uuidv4 } = require('uuid');

import { writeFile } from "fs";
import { resolve } from "path";
import Hero from "../../models/hero";

async function getHeroes() {
  const START_URL = "https://www.marvel.com/characters";
  const heroes: Hero[] = [];

  const browser = await puppeteer.launch({
    headless: true
  });
  const page = await browser.newPage();
  await page.setUserAgent(
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
  );
  
  console.log('Getting heroes from website', START_URL);
  await page.goto(START_URL);

  await page.waitForSelector("body");

  const aliases = await page.evaluate(() => {
    const querySelectorAll = document.querySelectorAll(".card-body__headline");
    return Array.from(querySelectorAll)
      .slice(0, 15)
      .map(item => (item as HTMLElement).innerText);
  });

  const names = await page.evaluate(() => {
    const querySelectorAll = document.querySelectorAll(".card-footer__secondary-text");
    return Array.from(querySelectorAll)
      .slice(0, 15)
      .map(item => (item as HTMLElement).innerText);
  });

  const photos = await page.evaluate(() => {
    const querySelectorAll = document.querySelectorAll(".card-thumb-frame img");
    return Array.from(querySelectorAll)
      .slice(0, 15)
      .map(item => (item as HTMLImageElement).src);
  });
  
  const bio_links = await page.evaluate(() => {
    const querySelectorAll = document.querySelectorAll(".mvl-card.mvl-card--explore a");
    return Array.from(querySelectorAll)
      .slice(0, 15)
      .map(item => (item as HTMLLinkElement).href);
  });

  const bios = [];
  for (let i = 0; i < bio_links.length; i++) {
    await page.goto(bio_links[i]);
    await page.waitForSelector("body");
    const bio = await page.evaluate(() => {
      const querySelector = document.querySelector(".masthead__copy") as HTMLElement;
      return querySelector == null ? '' : querySelector.innerHTML;
    });
    bios.push(bio);
  };

  aliases.forEach((hero: Hero, index: number) => {
    heroes.push({
      id: uuidv4(),
      alias: aliases[index],
      name: names[index],
      photo: photos[index],
      bio: bios[index]
    })
  });

  await browser.close();

  writeFile(
    resolve(__dirname, "../heroes.json"),
    JSON.stringify(heroes, null, 2),
    err => {
      if (err) {
        throw err;
      }
      console.log("Finished writing file");
    }
  );
}

getHeroes();
