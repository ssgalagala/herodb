import * as Express from "express";
import Hero from "../models/hero";
const fs = require('fs');
const path = require('path');

const findAll = async (req: Express.Request, res: Express.Response) => {
        // Return list of all characters from data crawled from website
        let rawdata = fs.readFileSync(path.join(__dirname, '../../src/data/heroes.json'));
        let heroes = JSON.parse(rawdata);
        res.json(heroes);
}

const findById = async (req: Express.Request, res: Express.Response) => {
        // Return 1 character (based on id) from data crawled from website
        let rawdata = fs.readFileSync(path.join(__dirname, '../../src/data/heroes.json'));
        let heroes = JSON.parse(rawdata);
        let hero = heroes.filter(function(hero: Hero) {
                return hero.id === req.params.id;
        })[0];
        res.json(hero);
}

export default {
    findAll,
    findById
}