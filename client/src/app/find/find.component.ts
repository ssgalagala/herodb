import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroService } from '../common/services/hero.service';
import {Location} from '@angular/common';
import { ACTORS } from '../data/data';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss']
})
export class FindComponent implements OnInit {
  character: any = {};
  history: string[] = [];
  actor: any = {};
 
  constructor(private heroService: HeroService, private route: ActivatedRoute,
              private location: Location, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.actor = ACTORS.filter(actor => {
        return actor.id === params.id;
      })[0];
      this.heroService.getHero(params.id)
        .subscribe(hero => {
          this.character = hero;
        });
    });
  }

  goBack() {
    this.history.pop();
    if (this.history.length > 0) {
      this.location.back();
    } else {
      this.router.navigateByUrl('/');
    }
  }
}
