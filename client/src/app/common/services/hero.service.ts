import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hero } from 'src/app/models/hero';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  constructor(private http: HttpClient) {}

  // Url that your server is running on
  private BASE_URL = 'http://localhost:3000';

  getHeroes(): Observable<Hero[]> {
    return this.http.get<any[]>(this.BASE_URL + '/heroes')
    .pipe(
      map(result => {
        return result.map(hero => {
          return {
            id: hero.id,
            knownAs: hero.alias,
            name: hero.name,
            description: hero.bio,
            imageUrl: hero.photo
          };
        });
      })
    );
  }

  getHero(id: string){
    return this.http.get<any>(this.BASE_URL + '/heroes/' + id)
    .pipe(
      map(result => {
        if (result) {
          return {
            id: result.id,
            knownAs: result.alias,
            name: result.name,
            description: result.bio,
            imageUrl: result.photo
          };
        }
      })
    );
  }
}
