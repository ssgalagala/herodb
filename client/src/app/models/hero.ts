export interface Hero {
    id: number;
    knownAs: string;
    name: string;
    description: string;
    imageUrl: string;
}
