export const ACTORS: any[] = [
    {
        id: 'f235957d-b5f4-432c-aa52-12a0e972b6ea',
        // tslint:disable-next-line: max-line-length
        photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/40/Captain_Marvel_trailer_at_the_National_Air_and_Space_Museum_4_%28cropped%29.jpg',
        actor: 'Brie Larson'
    },
    {
        id: 'cbfe69aa-18eb-46de-86dc-33e68b862763',
        photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ec/Josh_Brolin_SDCC_2014.jpg',
        actor: 'Josh Brolin'
    },
    {
        id: 'b6da8dde-d2d3-424a-84f9-da59711895c1',
        photoUrl: 'https://www.biography.com/.image/t_share/MTU0ODc4NDQ5OTM5MzkyMTkz/gettyimages-931925994-square.jpg',
        actor: 'Chadwick Boseman'
    },
    {
        id: '44896a31-b697-41ea-ad73-6fa96540505c',
        photoUrl: 'https://www.gstatic.com/tv/thumb/persons/71369/71369_v9_bb.jpg',
        actor: 'Jude Law'
    },
    {
        id: '0a0a6969-18ef-41aa-a95d-2073caaa7106',
        // tslint:disable-next-line: max-line-length
        photoUrl: 'https://www.biography.com/.image/t_share/MTQ4MTUwOTQyMDE1OTU2Nzk4/tom-holland-photo-jason-kempin-getty-images-801510482-profile.jpg',
        actor: 'Tom Holland'
    },
    {
        id: '8aa570a9-f2f8-4909-9dc1-21cca709592f',
        // tslint:disable-next-line: max-line-length
        photoUrl: 'https://cdn1.i-scmp.com/sites/default/files/styles/768x768/public/images/methode/2019/03/08/f4f9645c-415b-11e9-b20a-0cdc8de4a6f4_image_hires_152519.JPG?itok=6KD3aX-8&v=1552029923',
        actor: 'Reggie'
    },
    {
        id: '9b7dd211-17b7-4725-9b2e-4a81cd433c3b',
        photoUrl: 'https://m.media-amazon.com/images/M/MV5BNzg1MTUyNDYxOF5BMl5BanBnXkFtZTgwNTQ4MTE2MjE@._V1_UY1200_CR83,0,630,1200_AL_.jpg',
        actor: 'Robert Downey, Jr.'
    },
    {
        id: 'e33b950d-31b9-4daa-b934-bcf2df358f04',
        // tslint:disable-next-line: max-line-length
        photoUrl: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/chris-evans-at-the-avengers-age-of-ultron-press-conference-news-photo-1600175386.jpg',
        actor: 'Chris Evans'
    },
    {
        id: '0314a49c-528c-4558-9be0-1b6efc3f3a7c',
        photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/6/60/Scarlett_Johansson_by_Gage_Skidmore_2_%28cropped%29.jpg',
        actor: 'Scarlett Johansson'
    },
    {
        id: 'f6b83c75-54bc-44eb-9fd5-eec5f8e55640',
        photoUrl: 'https://www.gstatic.com/tv/thumb/persons/43389/43389_v9_bc.jpg',
        actor: 'Mark Ruffalo'
    },
    {
        id: 'e0019229-ba43-4efb-9c47-fb3b6d02c716',
        photoUrl: 'https://www.gstatic.com/tv/thumb/persons/31106/31106_v9_bb.jpg',
        actor: 'Jeremy Renner'
    },
    {
        id: '32105a7e-cca9-47d5-b8e9-e90365bcb264',
        photoUrl: 'https://www.gstatic.com/tv/thumb/persons/528854/528854_v9_bb.jpg',
        actor: 'Christopher Hemsworth'
    },
    {
        id: '7e31d4bc-668d-497f-8775-122abf71110f',
        photoUrl: 'https://terrigen-cdn-dev.marvel.com/content/prod/1x/3dman442.jpg',
        actor: ''
    },
    {
        id: 'b865d22e-ed70-45a3-b3c2-5d0e130baf49',
        photoUrl: 'https://terrigen-cdn-dev.marvel.com/content/prod/1x/triathlon.jpg',
        actor: ''
    },
    {
        id: '309df38a-c2e3-48fb-bed9-ee2a597de3d5',
        photoUrl: 'https://terrigen-cdn-dev.marvel.com/content/prod/1x/8-ball.jpg',
        actor: ''
    },
]