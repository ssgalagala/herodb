import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavComponent } from './nav.component';

export const ROUTES = [{ path: '', component: NavComponent }];


@NgModule({
  declarations: [NavComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)]
})
export class NavModule {}
